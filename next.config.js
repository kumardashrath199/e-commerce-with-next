/** @type {import('next').NextConfig} */
// const isProd = process.env.NODE_ENV === 'production'
  
const nextConfig = {
    images:{
        domains:['fakestoreapi.com','i.ibb.co']
    },
    // assetPrefix: isProd ? '/e-commerce-with-next/' : '',
    // trailingSlash: true,
    // output: 'export',
}

module.exports = nextConfig
