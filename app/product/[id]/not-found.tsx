export default function NotFound() {
    return (
      <div className="mt-24 flex mx-auto">
        <h2>Not Found</h2>
        <p>Could not find requested resource</p>
      </div>
    );
  }